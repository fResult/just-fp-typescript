export type IdentityFn<T> = (x: T) => T

// Composition as Identify - (f ํ g)(x) -> g(f(x))
export type CompositionFn = <T>(g: Function, f: Function) => IdentityFn<T>

export type FuncReturnBool<T> = (...args: Array<T>) => boolean