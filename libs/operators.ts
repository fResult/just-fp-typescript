import { IdentityFn } from './../@types/index';
import { CompositionFn, FuncReturnBool } from '@types'


export const not = function<T>(fn: FuncReturnBool<T>) {
  return function(...args: Array<T>) {
    return !fn(...args)
  }
}

const isEven = (val: number) => val % 2 === 0
const isOdd = not(isEven)

type AnyIdentityFn = (v: any) => any

// export const compose =
// (fn2: AnyIdentityFn, fn1: AnyIdentityFn) => (v: any) => fn2(fn1(v))

type TestFn = <T, U>(g: (x: T) => U, f: (x: any) => T) => (x: T) => U
export const compose: /*CompositionFn*/TestFn  = (g, f) => {
  return (x) => g(f(x))
}

// Function declaration
export function mod(y: number) {
  return function forX(x: number) {
    return x % y
  }
}

export const eq = (y: number) => (x: number) => x === y
