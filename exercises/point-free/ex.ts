// function output(txt: string): void {
  // console.log(txt)
// }

const output = console.log.bind(console)

type FnReturnBoolean = (...args: any[]) => boolean
type FnHasNoReturn = (...args: any[]) => void

const not = <T>(fn: FnReturnBoolean) =>
  (...args: T[]): boolean =>
    !fn(...args)

const when = (fn: FnHasNoReturn) => <T>(predicate: FnReturnBoolean) => (...args: T[]) => {
  if (predicate(...args)) {
    fn(...args)
  }
}

const printIf = when(output)

function isShortEnough(str: string): boolean {
  return str.length <= 5
}

// function isLongEnough(str: string): boolean {
  // return !isShortEnough(str)
// }
const isLongEnough = not<string>(isShortEnough)

const msg1 = 'Hello'
const msg2 = msg1 + ' World'

const shortEnoughData = printIf<string>(isShortEnough)
const longEnoughData = printIf<string>(isLongEnough)
shortEnoughData(msg1) // Hello
shortEnoughData(msg2)
longEnoughData(msg1)
longEnoughData(msg2) // Hello World

function isOdds(num:number) {
  return num % 2 === 1
}
const isEven = not(isOdds)
const oddsOrElse = printIf<number>(isOdds)
const evenOrElse = printIf<number>(isEven)
oddsOrElse(5) // 5
oddsOrElse(4)
evenOrElse(5)
evenOrElse(4) // 4

