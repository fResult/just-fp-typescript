import { pickServerRendezvous, pickServerSimple } from "./server-selection-strategies"
import { Server, Username } from "./types"

const serverSet1: Server[] = [
  "server0",
  "server1",
  "server2",
  "server3",
  "server4",
  "server5",
]

const serverSet2: Server[] = [
  "server0",
  "server1",
  "server2",
  "server3",
  "server4",
]

const usernames: Username[] = [
  "username0",
  "username1",
  "username2",
  "username3",
  "username4",
  "username5",
  "username6",
  "username7",
  "username8",
  "username9",
]

function seeResult(callbackSet1: (x: string) => string, callbackSet2: (x: string) => string) {
  return function forUsername(username: Username): string {
      const s1 = callbackSet1(username)
      const s2 = callbackSet2(username)
      const serversAreEqual = s1 === s2
      return `${username}: ${s1} => ${s2} | equal?: ${serversAreEqual}`
  }
}

const pickSimpleServerSet1 = pickServerSimple(serverSet1)
const pickSimpleServerSet2 = pickServerSimple(serverSet2)

const pickRendezvousServerSet1 = pickServerRendezvous(serverSet1)
const pickRendezvousServerSet2 = pickServerRendezvous(serverSet2)

const pickedServerSimple = usernames.map(seeResult(pickSimpleServerSet1, pickSimpleServerSet2))
const pickedServerRendezvous = usernames.map(seeResult(pickRendezvousServerSet1, pickRendezvousServerSet2))

console.log('Simple Hashing Strategy:', pickedServerSimple)
console.log('Rendezvous Hashing Strategy:', pickedServerRendezvous)
