import { Server, Username } from './types'
import * as utils from './utils'

export function pickServerSimple(servers: Server[]) {
  return function forUsername(username: Username): Server {
    const hash = utils.hashString(username)

    return servers[hash % servers.length]
  }
}

export function pickServerRendezvous(servers: Server[]) {
  return function forUsername(username: Username): Server {
    let maxServer: Server | null = null
    let maxScore: number | null = null

    for (const server of servers) {
      const score = utils.computeScore(username, server)
      if (maxScore === null || score > maxScore) {
        maxScore = score
        maxServer = server
      }
    }

    return maxServer!
  }
}
