;(function () {
  function strBuilder(str: string): Function {
    return function (v?: string) {
      if (typeof v === 'string') {
        return strBuilder(str + v)
      } else {
        return str
      }
    }
  }

  // const hello = strBuilder('Hello, ')
  // const kyle = hello('Kyle')
  // const susan = hello('Susan')
  // const question = kyle('?')()
  // const greeting = susan('!')()

  // console.log(strBuilder('Hello, ')('')('Kyle')('.')('')() === 'Hello, Kyle.')
  // console.log(hello() === 'Hello, ')
  // console.log(kyle() === 'Hello, Kyle')
  // console.log(susan() === 'Hello, Susan')
  // console.log(question === 'Hello, Kyle?')
  // console.log(greeting === 'Hello, Susan!')
})()
