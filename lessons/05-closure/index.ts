import { eq, mod, compose } from './../../libs/operators'
;(function () {
  /* DON'T */
  function makeCounter() {
    let counter = 0 // `counter` variable is closed
    return function increment() {
      return ++counter
    }
  }

  const countUp = makeCounter()

  console.log(countUp(), countUp(), countUp(), countUp(), countUp())

  /* DO */
  function unary(fn: Function) {
    // `fn` parameter is closed
    return function one<T>(arg: T) {
      return fn(arg)
    }
  }

  const isOdd = compose(eq(1), mod(2))
  const isOdd2 = unary(isOdd)
  console.log('3 is odd?:', isOdd2(3))
  console.log('4 is odd?:', isOdd2(4))

  /* more example */
  function addAnother(z: number) {
    // `z` parameter is closed
    return function add(x: number, y: number) {
      return x + y + z
    }
  }

  const addThree = addAnother(3)
  console.log('3 + (4 + 5):', addThree(4, 5))
})()
