import { compose, eq, mod, not } from '../../libs/operators';

const eq1 = eq(1)
const mod2 = mod(2)
// const isOdd = (x: number) => eq1(mod2(x))
const isOdd = compose(eq1, mod2)
const isEven = not(isOdd)

console.log('two is odd', isOdd(2))
console.log('five is odd', isOdd(5))
console.log('eight is even', isEven(8))
console.log('eleven is even', isEven(11))

