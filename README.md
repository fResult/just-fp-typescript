# Functional Programming in Typescript Learning
- from [Functional Javascript v3 - Frontend Masters](https://frontendmasters.com/courses/functional-javascript-v3) by Kyle Simpson
![Functional-light V3 - Book cover](/uploads/a1febaccd6e10b67aebf7204227b681b/fp-light.png)


### Provided script
```
yarn install #or
npm install
```

### Run any file
```bash
FILE=<PATH/TO/FILE> yarn nodemon # or
FILE=<PATH/TO/FILE> npx nodemon
```

#### Example
```bash
FILE=worksheets/test.ts yarn nodemon # or
FILE=worksheets/test.ts npx nodemon
```

#### The result in console will be...
```
This is test file!!
```

![Example file running](/uploads/7a263b8b18f36b315f4e6d2fe7ceb7f6/image.png)
