import { not } from '../../libs/operators'

function isOdds(num: number): boolean {
  return num % 2 === 1
}

const isEven = not<number>(isOdds)

console.log('Two is even?:', isEven(2))
