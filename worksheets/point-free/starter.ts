(function practice() {
  function run() {
    console.log('One is odd?:', isOdd(1))
    console.log('Two is odd?:', isOdd(2))
    console.log('Three is even?:', isEven(3))
    console.log('Four is even?:', isEven(4))
  }

  function isOdd(x: number) {
    return x % 2 === 1
  }

  const isEven = (x: number) => !isOdd(x)

  run()
})()
