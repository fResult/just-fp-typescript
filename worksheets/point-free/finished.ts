import { CompositionFn } from '../../@types'

;(function practice() {
  function run() {
    console.log('One is odd?:', isOdd(1))
    console.log('Two is odd?:', isOdd(2))
    console.log('Three is even?:', isEven(3))
    console.log('Four is even?:', isEven(4))
  }

  const eq = (y: number) => (x: number) => x === y
  const mod = (y: number) => (x: number) => x % y

  const compose: CompositionFn = (g, f) => {
    return (x) => g(f(x))
  }

  const isOdd = compose<number>(eq(1), mod(2))

  const isEven = not(isOdd) as (n: number) => boolean

  function not<T>(fn: Function) {
    return function (...args: T[]) {
      return !fn(...args)
    }
  }

  run()
})()
