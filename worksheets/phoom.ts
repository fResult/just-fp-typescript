import { compose } from './../libs/operators';
;(function () {
  type A = Record<string, any>

  type BaseProps = {
    role: string
  }

  type Person = {
    name: string
  }

  const person: Person | BaseProps = {
    role: 'Admin',
    name: 'Korn'
  }

  if ('role' in person) {
    console.log((person as BaseProps).role)
    console.log(person.role)
  } else if ('name' in person) {
    console.log((person as Person).name)
    console.log(person.name)
  }

  function getPerson(person: Person | BaseProps) {
    if ('role' in person) {
      person.role
    } else if ('name' in person) {
      person.name
    }
  }

  /* ========== */

  enum ImageOpenstackStatus {
    KILLED = 'killed',
    QUEUED = 'queued',
    UPLOADING = 'uploading',
    IMPORTING = 'importing',
    SAVING = 'saving',
    PENDING_DELETE = 'pending_delete',
    ACTIVE = 'active',
    DELETED = 'deleted',
    DEACTIVATED = 'deactivated'
  }
  type MappedStatuses = {
    [K in ImageOpenstackStatus]: K
  }
  const statuses: Partial<MappedStatuses> = {
    killed: ImageOpenstackStatus.KILLED
  }

  type Input = {
    title: {
      type: 'string'
    }
    age: {
      type: 'number'
    },
    members: {
      type: 'array',
      value: { type: 'number' }
    }
  }

  type Output = {
    title: string
    age: number
  }

  type TypeMapper<T> = {
    [K in keyof T]: T[K]
  }
  type MappedOutput<T extends { [K in keyof T]: { type: Scalar } }> = {
    [K in keyof T]: T[K]['type']
  }
  type Scalar = keyof TypeMapper
  type ToMappedOutput1<T = Input> = T extends { [key: string]: { type: Scalar } }? {
    [K in keyof T]: T[K] extends { type: keyof TypeMapper }
      ? TypeMapper[T[K]]
      : never
  }
  type TestOutput1 = ToMappedOutput1<Input>
  const t1: TestOutput1 = { title: 'Korn', age: 18 }
  const t11: ToMappedOutput1<number> = 3
  const tt1: ToMappedOutput1<Input> = {title: 0, age: 15}

  type Mystery<T> = T extends object ? {
    [K in keyof T]: T[K] extends { type: Scalar }
      ? 'yes'
      : 'no'
  } : never




  const myst: Mystery<number> = Number(3)
  myst //?
  // =============== */
  type ToMappedOutput2<T extends { [K in keyof T]: { type: Scalar } }> = {
    [K in keyof T]: TypeMapper[T[K]['type']]
  }
  type TestOutput2 = ToMappedOutput2<Input>
  const t2: TestOutput2 = { title: 'Korn', age: 18 }
  /* =============== */
  type ToMappedOutput3<T extends Record<string, { type: Scalar }>> = {
    [K in keyof T]: TypeMapper[T[K]['type']]
  }
  type TestOutput3 = ToMappedOutput3<Input>
  const t3: TestOutput3 = { age: 18, title: 'Korn' }
  /* =============== */
  type ToMappedOutput4<T> = T extends { [key: string]: { type: Scalar } }? {
    [K in keyof T]: TypeMapper[T[K]['type']]
  } : never
  const mappedOutput4Input: ToMappedOutput4<Input> = {
    title: 'hello',
    age: 18
  }
  const mappedOutput4Number: ToMappedOutput4<number>
  /* =============== */
  type ToMappedOutput5<T extends { [key: string]: { type: Scalar } }> = { [K in keyof T]: TypeMapper[T[K]['type']] }
  const mappedOutput5Input: ToMappedOutput5<Input> = {
    title: 'hello',
    age: 18
  }
  const mappedOutput5Number: ToMappedOutput5<number> //  compile error
  /* =============== */
  type ToMappedOutput6<T extends { [P in keyof T]: { type: Scalar } }> = {
    [K in keyof T]: TypeMapper[T[K]['type']]
  }

  const mappedOutput6Input: ToMappedOutput5<Input> = {
    title: 'hello',
    age: 18
  }
  const mappedOutput6WrongAddr: ToMappedOutput5<Input/*It should be an Address type*/> = {
    homeNo: 666,
    street: 'Wall Street'
  };
  const mappedOutput6Number: ToMappedOutput5<number> //  compile error
  /* =============== */
  type MapScalar<T> = T extends { type: Scalar }
    ? TypeMapper<T extends {[K in keyof T]: T[K]}? { [K in keyof T]: T[K] } : never>[T['type']]
    : never
    // = TypeMapper<
      // T extends { type: Scalar, value: string }
        // ? { [K in keyof T]: { type: T[K], value: T[K] } }
        // : { [K in keyof T]:  T[K] }
    // >

  type ToMappedOutput7<T extends { [P in keyof T]: { type: Scalar } }> = {
    [K in keyof T]: T[K] extends { type: 'array', value: infer Value }
      ? MapScalar<Value>
        : T[K] extends { type: Scalar }
        ? MapScalar<T[K]>
          : never
  }
  type Schema = {
    memberIds: {
      type: 'array',
      value: number[]
    },
    name: {
      type: 'string'
    }
  }

  const angles = [15, 30, 45, 60] as const
  const angs = 15 | 20 | 25
  type AAA = typeof angles[number]
  type BBB = { [K in AAA]: string }

  type GetArrayItem<T> = T extends Array<infer Element>
    ? Element
    : never
  type GetTupleItem<T> = T extends [infer First, infer Second, ...infer Tail]
    ? [First, Second, Tail]
    : never
  type GetObjectItem<T> = T extends { type: string, value: infer Value }
    ? Value
    : never
  type GetA = GetArrayItem<[10, 20, 30, 40]>
  type GetT = GetTupleItem<[10, 20, 30, 40]>
  type GetO = GetObjectItem<{type: string, value: string}>

  type MemberIDs = ToMappedOutput7<Schema>
  const memIds: MemberIDs = { name: 'Korn', memberIds: {type: 'array', value: 'array' }} }

  // const t = {
  // text: () => '',
  // number: () => 0,
  // many: <T>(input: T) => [input],
  // of: <T>(type: T) => type,
  // optional: <T>(type: T): T | null => type
  // }

  function ref<T>(type: T) {
    return { type: 'ref' as const, value: type }
  }

  ref({ type: 'Kornza' }).value //?

  const t = {
    type: <T>(name: string | number, schema: T) => ({
      type: 'schema',
      name,
      schema
    }),
    text: (): { type: 'string' } => ({ type: 'string' }),
    number: (): { type: 'number' } => ({ type: 'number' }),
    many: <T>(input: T): { type: 'array'; item: T } => ({
      type: 'array',
      item: input
    }),
    of: <T>(type: T): { type: 'ref'; item: T } => ({ type: 'ref', item: type }),
    optional: <T>(type: T): { type: 'optional'; item: T } => ({
      type: 'optional',
      item: type
    })
  }

  const Member = t.type('member', {
    id: t.text(),
    name: t.text(),
    age: t.number()
  })
  Member.schema.id //?
  const Team = t.type('team', {
    name: t.text(),
    lead: t.of(Member),
    members: t.many(t.of(t.of(Member)))
  })

  const korn: typeof Member = {
    id: 'XXXX-YYYY',
    name: 'Korn',
    age: 18
  }

  const team: Team = {
    name: 'Shuffle man',
    lead: korn,
    members: [korn]
  }

  const h = createHandler<Team>(() => {
    return {
      name,
      lead,
      members: [member]
    }
  })
})()
