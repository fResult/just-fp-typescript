type A = Record<string, any>;

type BaseProps = {
  role: string;
};

type Person = {
  name: string;
};

const person: Person | BaseProps = {
  role: "Admin",
  name: "Korn",
};

let result: unknown;
if ("role" in person) {
  result = (person as BaseProps).role;
  person.role;
}
if ("name" in person) {
  result = (person as Person).name;
}

function getPerson(person: Person | BaseProps) {
  if ("role" in person) {
    person.role;
  } else if ("name" in person) {
    person.name;
  }
}

const arr = [1024, 2048] as const;

type Tuple = [1024, 2048][number];

const isHello = <T extends string>(name: T): name is T => name === "hello";

function sayHello(name: string) {
  if (isHello(name)) {
    name; //?
    console.log("name is hello"); //?
  }
}

sayHello("hello");

function isDefined<T>(val: T): val is undefined {
  return val !== undefined;
}

const testValues = [undefined, null, "", [], {}, 0, false].map(isDefined);
testValues;

interface Book {
  id: number;
  name: string;
  isbn: string;
}

type BookButBoolean = {
  [P in keyof Book]: { test: Book[P]; new: number };
};

function setBookField<K extends keyof Book, V extends Book[K]>(
  key: K,
  value: V
) {
  return { [key]: value };
}

type Book2 = { [key: string]: string };

setBookField("isbn", "ksadfjisdkafji");

const angles: Record2<string, number> = { 2: 2, b: 3 };

type Record2<K extends keyof any, T> = {
  [P in keyof any]: T;
};

const angles2: Record3 = { a: 3 };
type Record3 = {
  [P in string]: number;
};

const angles3: Record4 = { a: 3 };
type Record4 = {
  [key: string]: any;
};

type Statuses = "pending" | "finished";
interface IStatus {
  pending: string;
  finished: string;
}
type Record5 = { [key in Statuses]: any };

const record: Record<Statuses, any> = { finished: 2, pending: "finished" };

type A2C = { a: 1; b: 2 };
type KA = keyof A2C;
type SS = `|${KA}|`;
type SSS = { [K in KA]: boolean };

type OptionalBook = Partial<Book>;
const optBook: OptionalBook = { id: 222 };

type Partial2<T> = {
  [P in keyof T]?: T[P];
};
type PBook = Partial2<Book>;

type Readonly2<T> = { readonly [K in keyof T]: T[K] };
type ROBook = Readonly<Book>;
type ROBook2 = Readonly2<Book>;

let arr2: readonly Book[] = [{ id: 2, isbn: "194ksdajf", name: "Korn" }];
arr2 = [...arr2, { id: 3, isbn: "2934891", name: "New Korn" }];
arr2 = arr2.filter((item) => item.id !== 3); //?

type PartialBook = Pick<Book, "id" | "name">;
type PartialBook2 = Pick2<Book, "id" | "isbn">;
type Pick2<T, K extends keyof T> = {
  [P in K]: T[P];
};

type Partial3 = Pick3<"id" | "name">;
type Pick3<K extends keyof Book> = {
  [P in K]: Book[P];
};

type LuckyNumbers = 44 | 112 | 66;
type LuckyNumbersWithout112 = Exclude2<LuckyNumbers, 112>;

type Exclude2<T, S> = T extends S ? never : T;

type CheckType<T> = T extends string
  ? "is string"
  : T extends number
  ? "is number"
  : T extends /*any[infer Element]*/ [infer Head, ...infer Tail]
  ? ["is array", Head, ...Tail]
  : "none at all";
type ResultA<T> = CheckType<T>;
type ResultB<T> = CheckType<T>;

const a: ResultA<3> = "is number";
const b: ResultB<"haha"> = "is string";
const c: CheckType<[50, 30, 40]> = ["is array", 50, 30, 40];

type MemberOfString = "true" extends string ? "yes" : "no";
