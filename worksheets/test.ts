console.log('This is test file!!')

type TPerson = {
  firstName: string
  lastName: string
  age: number
}
let persons: Array<TPerson> = [
  { firstName: 'foo', lastName: 'foofoo', age: 3 },
  { firstName: 'foo', lastName: 'foofoo', age: 4 },
  { firstName: 'foo', lastName: 'foof', age: 5 },
  { firstName: 'bar', lastName: 'foofoo', age: 6 }
]

function findPersonsAdvanceSearch<T, K extends keyof T>(
  sourceObj: Array<T>,
  objParams: Partial<T>
): Array<T> {
  let result: Array<T> = []
  const keys = Object.keys(objParams) as Array<K>

  keys.forEach(
    (key) =>
      (result = (sourceObj as unknown as T[]).filter((obj) => {
        if (objParams[key] === undefined) return false
        else if (objParams[key as K] === obj[key]) return true
        else return false
      }))
  )
  return result
}

console.log(
  findPersonsAdvanceSearch<TPerson, keyof TPerson>(persons ,{
    firstName: 'foo',
    lastName: 'foofoo',
    age: 4
  })
)

